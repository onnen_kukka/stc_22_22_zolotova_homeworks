import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int length;

        System.out.println("Введите размер массива:");

        length = scanner.nextInt();

        if (length < 2) {
            System.out.println("Введите число больше 1");
        } else {
            System.out.println("Длина массива " + length);
        }

        int[] array = new int[length];

        for (int i = 0; i < array.length; i++) {
            System.out.println("Введите число:");
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));

        int a = 0;

        for (int i = 0; i < array.length; i++) {

            if (i == 0) {
                if (array[i] < array[i + 1]) {
                    a++;
                }
            } else if (i == length - 1) {
                    if (array[i] < array[i - 1]) {
                        a++;
                    }
                } else if (array[i - 1] > array[i] && array[i] < array[i + 1]) {
                    a++;
                }
            }

        System.out.println("Количество локальных минимумов " + a);
    }

}
